let nbrCliqueGB = 0;
let nbrCliquePB1 = 0;
let nbrCliquePB2 = 0;
let checkBox2 = document.querySelector("#cbox2");
let texteSecret2 = document.querySelector("#texteSecret2");
let lesBoites = document.querySelectorAll(".boite");


function compterClique(uneId) {
    let scoreGB = document.querySelector("#scoreGB1");
    let scorePB1 = document.querySelector("#scorePB1");
    let scorePB2 = document.querySelector("#scorePB2");
    if (uneId == "grandeBoite1") {
        nbrCliqueGB += 1;
        scoreGB.innerHTML = nbrCliqueGB;
    } else if (uneId == "petiteBoite1") {
        nbrCliquePB1 += 1;
        scorePB1.innerHTML = nbrCliquePB1;
    } else if (uneId == "petiteBoite2") {
        nbrCliquePB2 += 1;
        scorePB2.innerHTML = nbrCliquePB2;
    }
}


for (let uneBoite of lesBoites) {
    uneBoite.addEventListener("click", function () {
        compterClique(uneBoite.id)
    })
}

checkBox2.addEventListener("change", function () {
    texteSecret2.classList.toggle("invisible");
})