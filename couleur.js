let btnAleaCouleur = document.querySelector("#aleaCouleur");
let btnAugCouleur = document.querySelector("#augmenter");
let inputCouleur = document.querySelector("#inputCouleur");
let afficheCouleur = document.querySelector("#afficheCouleur");


function affichageValeurCouleur() {
    if (localStorage.getItem("maCouleur")) {
        afficheCouleur.textContent = localStorage.getItem("maCouleur");
    }
}

function aleaHexa() {
    let nbr = Math.floor(Math.random() * 256);
    chaineHexa = nbr.toString(16);
    if (chaineHexa.length == 1) {
        chaineHexa == "0" + chaineHexa;
    }
    return chaineHexa;
}


function augmenterCouleur() {
    let couleur = document.body.style.backgroundColor;
    let chaineCouleur = couleur.slice(4, -1);
    let arrayCouleur = chaineCouleur.split(", ")
    // console.log(chaineCouleur);
    let nouveauRGB = [];
    for (let uneCouleur of arrayCouleur) {
        // console.log(uneCouleur)
        let newCouleur = parseInt(uneCouleur) + 10;
        if (newCouleur >= 255) {
            newCouleur = 255;
        }
        nouveauRGB.push(newCouleur)

    }
    let newChaineCouleur = composerCouleur(nouveauRGB);
    console.log(newChaineCouleur)
    document.body.style.backgroundColor = newChaineCouleur;
    localStorage.setItem("maCouleur", newChaineCouleur);
    affichageValeurCouleur();
}

function composerCouleur(unArray) {
    let resultat = "#";
    for (couleur of unArray) {
        let chaineHexa = couleur.toString(16);
        if (chaineHexa.length == 1) {
            chaineHexa = "0" + chaineHexa;
            resultat += chaineHexa;
        } else {
            resultat += chaineHexa;
        }
    }
    console.log(resultat);
    return resultat;
}

function changerAleaCouleur() {
    let rouge = aleaHexa();
    let vert = aleaHexa();
    let bleu = aleaHexa();
    let aleaCouleur = "#" + rouge + vert + bleu;
    localStorage.setItem("maCouleur", aleaCouleur);
    document.body.style.backgroundColor = aleaCouleur;
    affichageValeurCouleur();
}

if(localStorage.getItem("maCouleur"))
{
    affichageValeurCouleur();
    document.body.style.backgroundColor=localStorage.getItem("maCouleur");
}

inputCouleur.addEventListener("change", function () {
    localStorage.setItem("maCouleur", inputCouleur.value);
    document.body.style.backgroundColor=inputCouleur.value
    affichageValeurCouleur();
})

btnAleaCouleur.addEventListener("click", function () {
    let id = setInterval(changerAleaCouleur, 1000);
    setTimeout(function () {
        clearInterval(id)
    }, 10000);
})

btnAugCouleur.addEventListener("click", function () {
    let id = setInterval(augmenterCouleur, 1000)
    setTimeout(function () {
        clearInterval(id)
    }, 20000);
})